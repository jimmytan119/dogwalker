package io.wenjun.model.database

data class Dog(val id: String? = null, val name: String, var age: Int? = null, var description: String? = null, val ownerId: String? = null)