package io.wenjun.controller

import io.wenjun.ReactiveCrudAction
import io.wenjun.model.database.Dog
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
open class DogController {

  @Autowired
  @Qualifier("dogService")
  private lateinit var dogService: ReactiveCrudAction<Dog>

  @PostMapping(value = "/dogs", produces = [MediaType.APPLICATION_JSON_VALUE])
  @ResponseStatus(HttpStatus.CREATED)
  open fun create(@RequestBody dog: Dog): Mono<Dog> {
    return dogService.create(dog)
  }



}