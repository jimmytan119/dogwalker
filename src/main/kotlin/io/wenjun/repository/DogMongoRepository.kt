package io.wenjun.repository

import io.wenjun.model.database.Dog
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

interface DogMongoRepository : ReactiveMongoRepository<Dog, String> {
}