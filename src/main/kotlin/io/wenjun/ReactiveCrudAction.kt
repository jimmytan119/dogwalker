package io.wenjun

import reactor.core.publisher.Mono

interface ReactiveCrudAction<E> {
  fun create(entity: E): Mono<E>
  fun update(entity: E): Mono<E>
  fun get(id: String): Mono<E>
  fun delete(id: String)
}