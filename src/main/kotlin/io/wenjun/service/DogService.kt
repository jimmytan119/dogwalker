package io.wenjun.service

import io.wenjun.ReactiveCrudAction
import io.wenjun.model.database.Dog
import io.wenjun.repository.DogMongoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service("dogService")
open class DogService : ReactiveCrudAction<Dog> {

  @Autowired
  private lateinit var dogRepository: DogMongoRepository

  override fun create(entity: Dog): Mono<Dog> {
    return dogRepository.save(entity)
  }

  override fun update(entity: Dog): Mono<Dog> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun get(id: String): Mono<Dog> {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }

  override fun delete(id: String) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }
}