package io.wenjun

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class DogWalkerApplication

fun main(args: Array<String>) {
  SpringApplication.run(DogWalkerApplication::class.java, *args)
}